package loader

type Template func(map[string]string) *[]byte

func template(b *[]byte, m map[string]string) *[]byte {
	var t []byte
	c := 0
	l := len(*b)
	for c < l {
		if (*b)[c] == 0x24 {
			if (*b)[c+1] == 0x7b {
				c += 1
				s := c
				for (*b)[c] != 0x7d {
					c += 1
				}
				if v, ok := m[string((*b)[s:c])]; ok {
					t = append(t, []byte(v)...)
				}
			}
		}
		t = append(t, (*b)[c])
		c += 1
	}
	return &t
}

func NewTemplate(b []byte) Template {
	return func(m map[string]string) *[]byte {
		return template(&b, m)
	}
}
