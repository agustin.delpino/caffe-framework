module caffe-fw

go 1.19

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	go.uber.org/fx v1.19.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/dig v1.16.0 // indirect
	go.uber.org/multierr v1.9.0 // indirect
	go.uber.org/zap v1.24.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
)
