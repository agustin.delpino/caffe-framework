package caffe

import (
	"go.uber.org/fx"
)

type ApplicationProviders struct {
	Service Providers `yaml:"service"`
	Domain  Providers `yaml:"domain"`
	Infra   Providers `yaml:"infra"`
}

type AppConfig struct {
	Providers  *ApplicationProviders `yaml:"providers"`
	EntryPoint interface{}           `yaml:"entry-point"`
}

func StartApp(c *AppConfig) {
	fx.New(
		fx.Provide(append(c.Providers.Service,
			append(c.Providers.Domain, c.Providers.Infra...)...)...,
		),
		fx.Invoke(c.EntryPoint),
	).Run()
}

func StartAppWithEnvs[TEnvs any](c *AppConfig, e EnvsConfiguration[TEnvs]) {
	c.Providers.Infra = append(c.Providers.Infra, e)
	StartApp(c)
}
