package app

import (
	"caffe-fw/example/internal/items/infra/controller"
	inpututils "caffe-fw/example/pkg/input-utils"
	"caffe-fw/pkg/caffe"
	"fmt"
)

func Run(c caffe.IController[controller.ItemRequest]) error {
	for {
		cmd := inpututils.Inputf("enter command: ")
		if cmd != "item" {
			return fmt.Errorf("wrong input")
		}
		m := inpututils.Inputf("enter a id: ")

		c.Handle(&controller.ItemRequest{Id: m})
	}
}
