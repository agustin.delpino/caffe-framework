package caffe

type IController[TInput any] interface {
	Handle(*TInput)
}

type IService[TInput any, TOut any, TError error] interface {
	Do(*TInput) (*TOut, TError)
}

type IUseCase[TInput any, TOut any, TError error] interface {
	Do(*TInput) (*TOut, TError)
}
