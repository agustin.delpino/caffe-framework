package main

import (
	"caffe-fw/example/internal/app"
	"caffe-fw/example/internal/items/infra/controller"
	"caffe-fw/example/internal/items/service"
	"caffe-fw/pkg/caffe"
)

var appConfig = &caffe.AppConfig{
	Providers: &caffe.ApplicationProviders{
		Service: caffe.Providers{
			caffe.ProvideAs[caffe.IService[
				service.GetItemServiceRequest, service.GetItemServiceResponse, error]](service.New),
		},
		Infra: caffe.Providers{
			caffe.ProvideAs[caffe.IController[controller.ItemRequest]](controller.New),
		},
	},
	EntryPoint: app.Run,
}

func main() {
	caffe.StartApp(appConfig)
}
