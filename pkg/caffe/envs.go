package caffe

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type EnvsConfiguration[T any] func(string) Provider[T]

func NewConfig[T any](p string) Provider[T] {
	var c T
	if e := envconfig.Process(p, &c); e != nil {
		panic(errors.Wrap(e, "caffe.NewConfig"))
	}
	return func() *T {
		return &c
	}
}
