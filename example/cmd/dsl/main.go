package main

import (
	"caffe-fw/example/internal/app"
	"caffe-fw/example/internal/items/infra/controller"
	"caffe-fw/example/internal/items/service"
	"caffe-fw/pkg/caffe"
	"caffe-fw/pkg/caffe/dsl"
)

func main() {
	dsl.NewApp().
		AddServiceProvider(
			caffe.ProvideAs[caffe.IService[
				service.GetItemServiceRequest, service.GetItemServiceResponse, error]](service.New)).
		AddInfraProvider(caffe.ProvideAs[caffe.IController[controller.ItemRequest]](controller.New)).
		EntryPoint(app.Run).
		Start()
}
