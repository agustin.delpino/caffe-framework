package input_utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func Inputf(format string, a ...any) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf(format, a...)
	r, _ := reader.ReadString('\n')
	return strings.TrimSpace(r)
}
