package loader

var tplmaindsl = NewTemplate([]byte(`package main

${pkg}

func main() {
	dls.NewApp().
		${provider-service}
		${provider-domain}
		${provider-infra}
		EntryPoint(${entry-point}).
		Start()
}
`))

var tplprovider = NewTemplate([]byte(`Add${provider-type}(${provider-pkg}.New${provider-name})`))
var tplproviderAs = NewTemplate([]byte(`Add${provider-type}(caffe.ProvideAs[${provider-implementation},` +
	`${provider-interface}](${provider-pkg}.New${provider-name}))`))

var tplconstructor = NewTemplate([]byte(`func New${provider-name}(${provider-dependencies}) *${provider-implementation}{
	return &${provider-implementation}{
		${provider-injection}
	}
}`))
