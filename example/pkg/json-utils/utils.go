package json_utils

import (
	"encoding/json"
	"strings"
)

func Stringify(a interface{}, i int) string {
	b, err := json.MarshalIndent(&a, "", strings.Repeat(" ", i))
	if err != nil {
		panic(err)
	}
	return string(b)
}
