# Caffe Framework

# Quick examples
At the moment, there are two ways to create an application:
***Vanilla*** or ***DSL***.

The application is completely functional, just run the Boostrap you
want.

When the application executes, it will require you enter a command.
The only one avaiable command is: "item"
Then will aks you for an *id*, just enter `1234`.

# Goals
- Simplify the Application dependency configuration ✔️
- Full usage of interface ✔️
- Near approach to OOP ✔️
- Serve standard solution ❌ (*just Config and Injections by now*)
- Configure an Application by config file ❌
- DSL for configure an application ✔️
- Collection information at application building ❌
- CLI / Plugin / Live Templates for speed up the develop time ❌