package caffe

import "go.uber.org/fx"

type Provider[T any] func() *T
type Providers []interface{}

func ProvideAs[TAs any](p interface{}) interface{} {
	return fx.Annotate(p, fx.As(new(TAs)))
}
