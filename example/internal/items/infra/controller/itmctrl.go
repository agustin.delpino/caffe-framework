package controller

import (
	"caffe-fw/example/internal/items/service"
	json_utils "caffe-fw/example/pkg/json-utils"
	"caffe-fw/pkg/caffe"
	"fmt"
)

type ItemRequest struct {
	Id string
}

type ItemController struct {
	service caffe.IService[service.GetItemServiceRequest, service.GetItemServiceResponse, error]
}

func (c *ItemController) Handle(r *ItemRequest) {
	res, err := c.service.Do(&service.GetItemServiceRequest{Id: r.Id})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(json_utils.Stringify(res.Item, 2))
	}
}

func New(s caffe.IService[service.GetItemServiceRequest, service.GetItemServiceResponse, error]) *ItemController {
	return &ItemController{
		service: s,
	}
}
