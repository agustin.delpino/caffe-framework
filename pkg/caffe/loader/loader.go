package loader

import (
	"fmt"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
)

type provider struct {
	_interface string `yaml:"interface"`
	implements string `yaml:"implements"`
}

type providers struct {
	infra []provider `yaml:"infra"`
}

type constructor struct {
	injects        map[string]string `yaml:"injects"`
	dependencies   map[string]string `yaml:"dependencies"`
	implementation string            `yaml:"implementation"`
}

type app struct {
	constructors map[string]constructor `yaml:"constructors"`
	providers    *providers             `yaml:"providers"`
	entryPoint   string                 `yaml:"entry-point"`
}
type appLoader struct {
	app *app `yaml:"app"`
}

func LoadApp(p string) error {
	var a appLoader
	if b, err := os.ReadFile(p); err != nil {
		return errors.Wrapf(err, "caffe.loader.LoadApp: %s", p)
	} else if err := yaml.Unmarshal(b, &a); err != nil {
		if a.app == nil {
			return errors.New("caffe.loader.LoadApp: app field is missing")
		}

		// prvs := strings.Builder{}
		cnstr := strings.Builder{}

		if a.app.constructors != nil {
			for n, c := range a.app.constructors {
				if c.implementation == "" {
					return errors.New("caffe.loader.LoadApp: implementation field is missing")
				}
				inj := strings.Builder{}
				mapping := strings.Builder{}

				for p, t := range c.injects {
					inj.WriteString(fmt.Sprintf("%s %s,", p, t))
				}

				for f, p := range c.dependencies {
					mapping.WriteString(fmt.Sprintf("%s: %s,\n", f, p))
				}

				cnstr.WriteString(string(*tplconstructor(map[string]string{
					"provider-name":           n,
					"provider-dependencies":   inj.String(),
					"provider-implementation": c.implementation,
					"provider-injection":      mapping.String(),
				})) + "\n")
			}
		}

	}
	return nil
}
