package service

import (
	"caffe-fw/example/internal/items/domain"
	"github.com/pkg/errors"
)

type GetItemServiceRequest struct {
	Id string
}
type GetItemServiceResponse struct {
	Item *domain.Item
}

type GetItemService struct{}

func (s GetItemService) Do(r *GetItemServiceRequest) (*GetItemServiceResponse, error) {
	if r.Id != "1234" {
		return nil, errors.New("internal.items.service.GetItemService: not found item")
	}

	return &GetItemServiceResponse{
		Item: &domain.Item{
			Id:   r.Id,
			Type: "Foo",
		},
	}, nil

}

func New() *GetItemService {
	return &GetItemService{}
}
