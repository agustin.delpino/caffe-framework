package domain

type Item struct {
	Id   string `json:"id"`
	Type string `json:"type"`
}
