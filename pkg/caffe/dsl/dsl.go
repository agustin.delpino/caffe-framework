package dsl

import "caffe-fw/pkg/caffe"

type App struct {
	config *caffe.AppConfig
}

func (a *App) AddServiceProvider(p interface{}) *App {
	a.config.Providers.Service = append(a.config.Providers.Service, p)
	return a
}

func (a *App) AddDomainProvider(p interface{}) *App {
	a.config.Providers.Domain = append(a.config.Providers.Domain, p)
	return a
}

func (a *App) AddInfraProvider(p interface{}) *App {
	a.config.Providers.Domain = append(a.config.Providers.Domain, p)
	return a
}

func (a *App) EntryPoint(p interface{}) *App {
	a.config.EntryPoint = p
	return a
}

func (a *App) Start() {
	caffe.StartApp(a.config)
}

func NewApp() *App {
	return &App{config: &caffe.AppConfig{
		Providers: &caffe.ApplicationProviders{},
	}}
}

type AppWithEnvs[T any] struct {
	App
	envs caffe.EnvsConfiguration[T]
}

func (a *AppWithEnvs[T]) Envs(p caffe.EnvsConfiguration[T]) *AppWithEnvs[T] {
	a.envs = p
	return a
}

func (a *AppWithEnvs[T]) Start() {
	caffe.StartAppWithEnvs(a.config, a.envs)
}
